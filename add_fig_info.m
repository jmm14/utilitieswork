function add_fig_info(mname,info,options)
% Add text box to figure with basic info
%
% Justine McMillan
% Jul 15, 2022

if ~isfield(options,'fontSize'); options.fontSize = 6; end
if ~isfield(options,'position'); options.position = [0, 0.08, 0.9, 0]; end
warning off

text = {info,['Created: ',datestr(datetime)],mname};

annotation('textbox',options.position,'interpreter','none','string', text,'FontSize',options.fontSize,'EdgeColor','None')

warning on
