function C=catstructs_JMM(A,B,varargin)
% A and B are stuctures with the same fields
% varargin{1} = the dimension to concatenate along
% varargin{2} = the length of the dimension of B that you want to concatenate
% together
% Justine McMillan
% Oct 28, 2013
%
% August 12, 2014 - Modified to work with adcp structs (added varargin)
% June 5, 2015 -- Modified to deal with fields of multiple shapes and sizes
% Oct 16, 2015 -- Created a bit of a hack to deal with special cases where
%                 the number of columns and dimensions are the same. 

fields=fieldnames(A);
C = struct();
if nargin == 2
    dim = 0;
    nEL = NaN;
    warning('This function has been modified and may not work on codes older than June 5, 2015')
elseif nargin == 3
    dim = varargin{1};
    nEL = size(B.(fields{1}),dim);
    warning('This function has been modified and may not work on codes older than June 5, 2015')
else
    dim = varargin{1}; %will be overwritten
    nEL = varargin{2};
end


for ii=1:numel(fields)
    Afield = A.(fields{ii});
    Bfield = B.(fields{ii});
    
    [nrA,ncA,ndA] = size(Afield);
    [nrB,ncB,ndB] = size(Bfield);
    
    if nrB == nEL
        C.(fields{ii}) = cat(1,Afield,Bfield);
    elseif ncB == nEL 
        if ndB~=nEL % fudge to deal with cases where second and third dim
                    % are the same
            C.(fields{ii}) = cat(2,Afield,Bfield);
        else
            warning('Forcing to concatenate along third dimension')
            fields{ii}
            C.(fields{ii}) = cat(3,Afield,Bfield);
        end
        
    elseif ndB == nEL
        C.(fields{ii}) = cat(3,Afield,Bfield);
    else
        C.(fields{ii}) = [Afield  Bfield];
    end

end


