function disp_percdone(count,total,interval,varargin)
%% function disp_percdone(count,total,interval)
% Displays the percentage of the way through the loop the code is
%
% Inputs:
% - count: current iteration number
% - total: total number of iterations to complete
% - interval: interval at which to display output
%
% Justine McMillan
% Feb 10, 2015
if nargin == 4;
    textcol = varargin{1};
else
    textcol = 'black';
end

dispvec = [0:interval:100];

percdone = 100*count/total;
val_prev = 100*(count-1)/total;
val_next = 100*(count+1)/total;


d = min(abs(dispvec-percdone));
d_prev = min(abs(dispvec-val_prev));
d_next = min(abs(dispvec-val_next));

if d<d_prev & d<d_next
    string=['Percent done... ',num2str(round(percdone)),'%% \n'];
    cprintf(textcol,   string)
end