function metadata = get_metadata(progname,varargin)
%GET METADATA
% metadata = get_metadata(progname,varargin)
%
% Function to get the metadata and store it in a structure
% Justine McMillan
% July 4, 2014
%
% Sep 9, 2014 - Added optional input of metadata struct to which new data
% can be added

if nargin == 2
    metadata_old = varargin{1};
    
    metadata = metadata_old;
    
    ind = strmatch(progname, {metadata_old.progname}, 'exact');
    if ind>0
        disp('Metadata exists - updating date only')
        
        metadata(ind).date = datestr(now);
    else
        disp('Adding new data to existing metadata struct')
        nm = length(metadata_old);
        metadata(nm+1).progname = progname;
        metadata(nm+1).author = 'Justine McMillan';
        metadata(nm+1).date = datestr(now);
    end
    
else
    
    
    metadata.progname = progname;
    metadata.author = 'Justine McMillan';
    metadata.date = datestr(now);
end