function [xOut,ind] = remove_nans(x)
% Simple function to remove nans from a vector
%
% Justine McMillan
% Mar 11, 2022

ind = ~isnan(x);
xOut = x(ind);

