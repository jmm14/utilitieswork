function [y0upper, y0lower , x0, y0, beta] = plot_CI_regression_line(CI,betaIn,xi,yi,options)
%% [y0upper, y0lower , x0, y0, beta] = plot_CI_regression_line(CI,beta,x,y,varargin)
%
% Creates two curves marking the CI confidence interval for the linear
% least squares regression line. Will also check betaIn values to computed
% regression based on xi,yi
%
% Inputs:
% - CI: Confidence limit (e.g. 0.95)
% - betaIn: (Optional) Regression coefficients, betaIn(1) = intercept, betaIn(2) = slope.
% - xi: input x data
% - yi: input y data
% - options: Structure with various options
%    * plotLines: Plot lines with this function (1 = True)
%    * nPts: Number of points at which the funnel plot is defined. Default = 100
%    * xRange: [xMin,Max] for thee range of x values over which the funnel plot is defined
%
% Outputs:
% - y0upper: y coordinates for upper confidence interval
% - y0lower: y coordinates for lower confidence interval
% - x0: x coordinates for regression line and confidence intervals
% - y0: y coordinates for regression line
% - beta: Computed beta values
%
% Examples:
%   [y0upper, u0lower] = plot_CI_regression_line(0.95,[0.2 0.02] ,xi,yi,struct('nPts',100,'xRange',[0, 3]))
%   [y0upper, u0lower] = plot_CI_regression_line(0.95,[] ,xi,yi,struct('nPts',100,'xRange',[0, 3]))
%
% Note: Outputs have been compared to Matlab's interal functions and the
% same confidence intervals were obtained. More specifically:
%   mdl = fitlm(x,y,'linear');
%   [y0 ci] = predict(mdl,xfit','Alpha',1-CI); % ci is [y0lower; y0upper]
%
% TODO: Could be extended to provide CIs for multivariate linear
% regression. Currently only works for order 1 based on the way x0 is
% defined. Would need to allow x0 to be an input to allow for more than one
% independent variable. Low priority.
%
% 2022-03-16, JMM: Downloaded from Mathworks
% 2022-03-16, JMM: Modified to take CI as input and only plot CI lines
% 2023-01-24, JMM: Removed nans, 
%                  Corrected CI by using t-distribution
%                  Converted equations to matrix form
%                  Added calculation of beta, so betaIn is not actually a
%                  required input
%

% Setup
if ~isfield(options,'plotLines'); options.plotLines = 0; end
if ~isfield(options,'nPts'); options.nPts = 100; end
if ~isfield(options,'xRange'); options.xRange = [min(xi) max(xi)]; end

Nobs = length(xi);

% Transpose if necessary, xin and yin should have size Nobs x 1
[nr,nc] = size(yi); if nr ~= Nobs; yi = yi'; end
[nr,nc] = size(xi); if nr ~= Nobs; xi = xi'; end

% Remove nans
indGood = find(~isnan(xi) & ~isnan(yi));
xi = xi(indGood);
yi = yi(indGood);

Nobs = length(xi);

% Set up fit variables
Nfit = options.nPts;
xMin = options.xRange(1);
xMax = options.xRange(2);
x0 = linspace(xMin,xMax,Nfit)'; 
%yfit = ones(size(xfit))*beta(1) + beta(2)*xfit;


%  Multivariate linear regression analysis  
X = [ones(Nobs,1) xi]; % [1 x1; 1 x2; 1 x3; ...; 1 xN]. Has size Nobs x 2.
beta = X\yi; % X\yin = inv(X'*X)*X'*yi (estimators for multiple linear regression)

% Check calculated beta compared to input beta
if ~isempty(betaIn)
    [nr,nc] = size(betaIn);
    if nr == 1; betaIn = betaIn'; end
    if any(abs(beta - betaIn)>1e-5)
        warning('beta values do not agree'),pause
    end
end

% Estimate confidence limits [y0 +/- tcrit * sqrt(varY0)]
alpha = 1-CI;
df    = Nobs-2; % equivalent to Nobs-(k+1) where k is the number of independent variables (k=1 for linear regression)
tcrit = tinv(1-alpha/2,df); % critical value of the t distribution. Will asymptote to 1.96 for CI=0.95 as Nobs increases

yhat = X*beta; % modelled values based on regression
varRes = sum((yi-yhat).^2)/(Nobs-2); % variance of residuals. sometimes written in matrix form as (y-yhat)'*(y-yhat)/(N-2). N-2 is the number of degrees of freedom

X0 = [ones(Nfit,1) x0]; % [1 x0(1); 1 x0(2); 1 x0(3); ...; 1 x0(N)]. Has size Nfit x 2.
varY0 = NaN*ones(Nfit,1);
for ii = 1:Nfit
    X0vec = X0(ii,:);
    varY0(ii) = varRes*X0vec*inv(X'*X)*X0vec'; % Variance about Y0 
end

% 
y0 = X0*beta;
y0upper = y0 + tcrit*sqrt(varY0);
y0lower = y0 - tcrit*sqrt(varY0);

%% plot
if options.plotLines
    plot(x0,y0,'red','LineWidth',2);
    plot(x0,y0upper,'green--','LineWidth',2);
    plot(x0,y0lower,'green--','LineWidth',2);
    hold off
end

    


