function [YDT,P] = detrend_with_NaNs(Y,x,options)
% Function to remove the inear trend from each column of the input matrix X. 
% Trying reproduce detrend(X,'omitnan') which is available in newer
% versions of Matlab.
%
% Inputs:
% - Y: Matrix to detrend
% - x: Vector for the x values (if empty, data will be assumed to be evenly spaced)
% - options: Structure containing various options
%    * Regress1D: Simply drop nans and do linear regression
%    * Poly1D: Fit one dimensional polynomial (requires x vector)
%   
%
% Outputs:
% - YDT: Matrix with detrended values
% - P: Matrix of polnomial coefficients.
%
% Note: Regress1D and Poly1D should give the same results but a simple test
% shows that Regress1D is 4 times faster.
%
% Justine McMillan
% Mar 11, 2022

% Defaults
if ~isfield(options,'method'); options.method = 'Regress1D'; end
if ~isfield(options,'showOutput'); options.showOutput = 1; end

% Initialize
[NR,NC]=size(Y);
YDT = NaN*ones(NR,NC);
P = NaN*ones(2,NC); % Since this only uses 1D fits
if isempty(x)
    if options.showOutput
        disp('Assuming evening spaced points')
    end
    x = 1:NR;
end
xRel = x-x(1); %Use relative values for x values

% Checks
methods = {'Regress1D','Poly1D'};
if sum(ismember(options.method,methods)) == 0
    disp(['Detrending: ',options.method,' method not implemented. Returning nans'])
    return
end
[NRx,NCx] = size(xRel);
if NCx == 1
    disp('Transposing')
    xRel = xRel';
end

% Regression matrix
X = [ones(size(xRel))' xRel']; % Subtract off first value to get relative values

for cc = 1:NC
    y = Y(:,cc);
    ind = find(~isnan(y));
    
    if strcmp(options.method,'Regress1D')
        P(:,cc) = X(ind,:)\y(ind);
        trend = X*P(:,cc);
        YDT(:,cc) =  y - trend;   
        
    elseif strcmp(options.method,'Poly1D')
        y = y';
        P(:,cc) = polyfit(xRel(ind),y(ind),1);
        trend = polyval(P(:,cc),xRel);
        YDT(:,cc) =  y - trend;
         
    end
    
    if 0 % Debugging figure
        figure(1),clf
        plot(xRel,y)
        hold all
        plot(xRel,YDT(:,cc))
        plot(xRel,trend,'linewidth',3)
        plot(xRel,0*xRel,'k')
        title(num2str(P(:,cc)))
        legend('data','detrended','trend')
        pause
    end

end