function ind = get_nearestindex(vec,val);
% Function to get index of nearest point
%
% vec = vector inquiring about
% val = value searching for
%
% ind = index of vec that is closest to val
%
% Justine McMillan
% Dec 4, 2013

[~,ind] = min(abs(vec-val));