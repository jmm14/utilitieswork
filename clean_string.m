function strOut = clean_string(strIn)
% Clean a string so it appears nicely on plots
%
% Justine McMillan
% Jul 14, 2022

% remove underscore
strOut = strrep(strIn, '_', '\_');