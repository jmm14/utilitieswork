function ind = get_ind_in_range(vec, valMin, valMax)
% Get the indices of a vector that are within a certain range. Useful when
% extracting raw data for a given segment.
%
% Justine McMillan
% 2021-01-14

ind = find(vec>=valMin & vec<=valMax);



