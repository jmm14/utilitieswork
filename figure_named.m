function figure_named(FNAME,varargin)
% March 2012
% 
% Changes:
% - Mar 22, 2016: Added optional input to clear figure
   

fignum=findobj('name',FNAME);

if isempty(findobj('name',FNAME))
    figure('Name',FNAME)
else
    figure(fignum)
end

if nargin == 2
    if varargin{1} == 1
        clf
    end
end